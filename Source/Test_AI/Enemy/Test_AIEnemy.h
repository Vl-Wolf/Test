﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "GameFramework/Character.h"
#include "Test_AI/Ability/Base/TestAI_AbilityBase.h"
#include "Test_AI/Ability/Base/TestAI_AbilitySystemComponent.h"
#include "Test_AI/Ability/Base/TestAI_Attribute.h"
#include "Test_AI/ActorComponent/HealthComponent.h"
#include "Test_AI/ActorComponent/InventoryComponent.h"
#include "Test_AIEnemy.generated.h"

UCLASS()
class TEST_AI_API ATest_AIEnemy : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATest_AIEnemy();

	virtual UTestAI_AbilitySystemComponent* GetAbilitySystemComponent() const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Health", meta=(AllowPrivateAccess="true"))
	UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Health", meta=(AllowPrivateAccess="true"))
	UInventoryComponent* InventoryComponent;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="AbilitySystem", meta=(AllowPrivateAccess="true"))
	UTestAI_AbilitySystemComponent* AbilitySystemComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="GameplayAbilities")
	TArray<TSubclassOf<UTestAI_AbilityBase>> DefaultAbilities;
		
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="BaseAttribute")
	const UTestAI_Attribute* BaseAttribute;

	UFUNCTION(BlueprintPure, Category="BaseAttribute")
	void GetHealthValue(float& Health, float& MaxHealth);

	UFUNCTION(BlueprintPure, Category="BaseAttribute")
	void GetActionPointsValue(float& ActionPoints, float& MaxActionPoints);

	void OnHealthChangeNative(const FOnAttributeChangeData& Data);
	void OnActionPointsChangeNative(const FOnAttributeChangeData& Data);

	UFUNCTION(BlueprintImplementableEvent, Category="BaseAttribute")
	void OnHealthChange(float OldValue, float NewValue);
	UFUNCTION(BlueprintImplementableEvent, Category="BaseAttribute")
	void OnActionPointsChange(float OldValue, float NewValue);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="GameplayTag")
	FGameplayTag GameplayTagCharacter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="GameplayTag")
	FGameplayTag GameplayTagSend;
	

	UFUNCTION(BlueprintCallable)
	void Attack();
};
