﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Test_AIEnemy.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
ATest_AIEnemy::ATest_AIEnemy()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	AbilitySystemComponent = CreateDefaultSubobject<UTestAI_AbilitySystemComponent>(TEXT("AbilitySystemComponent"));

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

UTestAI_AbilitySystemComponent* ATest_AIEnemy::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

// Called when the game starts or when spawned
void ATest_AIEnemy::BeginPlay()
{
	Super::BeginPlay();

	if (AbilitySystemComponent)
	{
		BaseAttribute = AbilitySystemComponent->GetSet<UTestAI_Attribute>();

		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(BaseAttribute->GetHealthAttribute()).AddUObject(this,
																			&ATest_AIEnemy::OnHealthChangeNative);

		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(BaseAttribute->GetActionPointsAttribute()).AddUObject(this,
																			&ATest_AIEnemy::OnActionPointsChangeNative);
	}

	AbilitySystemComponent->AddLooseGameplayTag(GameplayTagCharacter);
}

float ATest_AIEnemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (HealthComponent)
	{
		HealthComponent->ChangeHeathValue(-DamageAmount);
	}


	return ActualDamage;
}

// Called every frame
void ATest_AIEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATest_AIEnemy::GetHealthValue(float& Health, float& MaxHealth)
{
	Health = BaseAttribute->GetHealth();
	MaxHealth = BaseAttribute->GetMaxHealth();
}

void ATest_AIEnemy::GetActionPointsValue(float& ActionPoints, float& MaxActionPoints)
{
	ActionPoints = BaseAttribute->GetActionPoints();
	MaxActionPoints = BaseAttribute->GetMaxActionPoints();
}

void ATest_AIEnemy::OnHealthChangeNative(const FOnAttributeChangeData& Data)
{
	OnHealthChange(Data.OldValue, Data.NewValue);
}

void ATest_AIEnemy::OnActionPointsChangeNative(const FOnAttributeChangeData& Data)
{
	OnActionPointsChange(Data.OldValue, Data.NewValue);
}

void ATest_AIEnemy::Attack()
{
	FVector AttackLocation = GetActorLocation();
	FVector AttackForwardVector = GetCapsuleComponent()->GetForwardVector() * 120.0f;
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Pawn));
	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(this);
	UClass * ActorClassFilter = nullptr;
	TArray<AActor*> OutActors;
	FRotator Rotator = FRotator::ZeroRotator;

	DrawDebugCapsule(GetWorld(), AttackLocation + AttackForwardVector, 96.0f, 42.0f,
							FQuat(Rotator), FColor::Blue, false, 2.0f, 0.0f, 1.0f);
	UKismetSystemLibrary::CapsuleOverlapActors(GetWorld(), AttackLocation + AttackForwardVector, 96.0f,
										42.0f, ObjectTypes, ActorClassFilter, IgnoredActors, OutActors);

	for(AActor* OverlapActors : OutActors)
	{
		FGameplayEventData GameplayEventData;
		GameplayEventData.Instigator = GetInstigator();
		GameplayEventData.Target = OverlapActors;
		GameplayEventData.TargetData = UAbilitySystemBlueprintLibrary::AbilityTargetDataFromActor(OverlapActors);
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetInstigator(), GameplayTagSend, GameplayEventData);
	}
}

