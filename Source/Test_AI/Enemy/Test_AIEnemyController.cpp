﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Test_AIEnemyController.h"

// Sets default values
ATest_AIEnemyController::ATest_AIEnemyController()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATest_AIEnemyController::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATest_AIEnemyController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
