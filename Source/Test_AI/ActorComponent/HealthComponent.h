﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TEST_AI_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Health")
	FOnHealthChange OnHealthChange;

protected:

	float Health = 100.0f;

public:

	UFUNCTION(BlueprintCallable, Category="Health")
	float GetCurrentHealth(float ChangeValue);

	UFUNCTION(BlueprintCallable, Category="Health")
	void ChangeHeathValue(float ChangeValue);

	UFUNCTION(BlueprintCallable, Category="Health")
	void HealthChange(float NewHealth, float Value);
};
