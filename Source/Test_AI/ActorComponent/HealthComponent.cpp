﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"


float UHealthComponent::GetCurrentHealth(float ChangeValue)
{
	return Health;
}

void UHealthComponent::ChangeHeathValue(float ChangeValue)
{
	Health += ChangeValue;

	HealthChange(Health, ChangeValue);

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			UE_LOG(LogTemp, Warning, TEXT("DEAD!"))
		}
	}
}

void UHealthComponent::HealthChange(float NewHealth, float Value)
{
	OnHealthChange.Broadcast(NewHealth, Value);
}

