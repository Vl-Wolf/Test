// Copyright Epic Games, Inc. All Rights Reserved.

#include "Test_AICharacter.h"

#include "BlueprintEditor.h"
#include "CollisionDebugDrawingPublic.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Test_AI/Test_AI.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Abilities/GameplayAbility.h"
#include "AbilitySystemBlueprintLibrary.h"

ATest_AICharacter::ATest_AICharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));

	AbilitySystemComponent = CreateDefaultSubobject<UTestAI_AbilitySystemComponent>(TEXT("AbilitySystemComponent"));
}

void ATest_AICharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

void ATest_AICharacter::BeginPlay()
{
	Super::BeginPlay();

	InitDefaultAbilities();

	if (AbilitySystemComponent && InputComponent)
	{
		const FGameplayAbilityInputBinds InputBinds(
			"Confirm",
			"Cancel",
			"EGASAbilityInputID",
			static_cast<int32>(EGASAbilityInputID::Confirm),
			static_cast<int32>(EGASAbilityInputID::Cancel));
		AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent, InputBinds);
	}

	if (AbilitySystemComponent)
	{
		BaseAttribute = AbilitySystemComponent->GetSet<UTestAI_Attribute>();

		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(BaseAttribute->GetHealthAttribute()).AddUObject(this,
																			&ATest_AICharacter::OnHealthChangeNative);

		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(BaseAttribute->GetActionPointsAttribute()).AddUObject(this,
																			&ATest_AICharacter::OnActionPointsChangeNative);
	}

	AbilitySystemComponent->AddLooseGameplayTag(GameplayTagCharacter);
}

float ATest_AICharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                                    AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (HealthComponent)
	{
		HealthComponent->ChangeHeathValue(-DamageAmount);
	}


	return ActualDamage;
	
}

UTestAI_AbilitySystemComponent* ATest_AICharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ATest_AICharacter::InitDefaultAbilities()
{
	if (AbilitySystemComponent)
	{
		for (TSubclassOf<UTestAI_AbilityBase>& Ability : DefaultAbilities)
		{
			int32 InputID = static_cast<int32>(Ability.GetDefaultObject()->AbilityInputID);
			
			AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(Ability, 1, InputID, this));
		}
	}
}

void ATest_AICharacter::GetHealthValue(float& Health, float& MaxHealth)
{
	Health = BaseAttribute->GetHealth();
	MaxHealth = BaseAttribute->GetMaxHealth();
}

void ATest_AICharacter::GetActionPointsValue(float& ActionPoints, float& MaxActionPoints)
{
	ActionPoints = BaseAttribute->GetActionPoints();
	MaxActionPoints = BaseAttribute->GetMaxActionPoints();
}

void ATest_AICharacter::OnHealthChangeNative(const FOnAttributeChangeData& Data)
{
	OnHealthChange(Data.OldValue, Data.NewValue);
}

void ATest_AICharacter::OnActionPointsChangeNative(const FOnAttributeChangeData& Data)
{
	OnActionPointsChange(Data.OldValue, Data.NewValue);
}

void ATest_AICharacter::Attack()
{
	FVector AttackLocation = GetActorLocation();
	FVector AttackForwardVector = GetCapsuleComponent()->GetForwardVector() * 120.0f;
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Pawn));
	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(this);
	UClass * ActorClassFilter = nullptr;
	TArray<AActor*> OutActors;
	FRotator Rotator = FRotator::ZeroRotator;

	DrawDebugCapsule(GetWorld(), AttackLocation + AttackForwardVector, 96.0f, 42.0f,
							FQuat(Rotator), FColor::Blue, false, 2.0f, 0.0f, 1.0f);
	UKismetSystemLibrary::CapsuleOverlapActors(GetWorld(), AttackLocation + AttackForwardVector, 96.0f,
										42.0f, ObjectTypes, ActorClassFilter, IgnoredActors, OutActors);

	for(AActor* OverlapActors : OutActors)
	{
		FGameplayEventData GameplayEventData;
		GameplayEventData.Instigator = GetInstigator();
		GameplayEventData.Target = OverlapActors;
		GameplayEventData.TargetData = UAbilitySystemBlueprintLibrary::AbilityTargetDataFromActor(OverlapActors);
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetInstigator(), GameplayTagSend, GameplayEventData);
	}
}
