// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Test_AI/ActorComponent/HealthComponent.h"
#include "Test_AI/ActorComponent/InventoryComponent.h"
#include "AbilitySystemInterface.h"
#include "Test_AI/Ability/Base/TestAI_AbilityBase.h"
#include "Test_AI/Ability/Base/TestAI_AbilitySystemComponent.h"
#include "Test_AI/Ability/Base/TestAI_Attribute.h"
#include "Test_AICharacter.generated.h"

UCLASS(Blueprintable)
class ATest_AICharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	ATest_AICharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	virtual void BeginPlay() override;

	virtual UTestAI_AbilitySystemComponent* GetAbilitySystemComponent() const override;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Health", meta=(AllowPrivateAccess="true"))
	UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Inventory", meta=(AllowPrivateAccess="true"))
	UInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="AbilitySystem", meta=(AllowPrivateAccess="true"))
	UTestAI_AbilitySystemComponent* AbilitySystemComponent;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	virtual void InitDefaultAbilities();

	UPROPERTY(EditDefaultsOnly, Category="GameplayAbilities")
	TArray<TSubclassOf<UTestAI_AbilityBase>> DefaultAbilities;

public:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="BaseAttribute")
	const UTestAI_Attribute* BaseAttribute;

	UFUNCTION(BlueprintPure, Category="BaseAttribute")
	void GetHealthValue(float& Health, float& MaxHealth);

	UFUNCTION(BlueprintPure, Category="BaseAttribute")
	void GetActionPointsValue(float& ActionPoints, float& MaxActionPoints);

	void OnHealthChangeNative(const FOnAttributeChangeData& Data);
	void OnActionPointsChangeNative(const FOnAttributeChangeData& Data);

	UFUNCTION(BlueprintImplementableEvent, Category="BaseAttribute")
	void OnHealthChange(float OldValue, float NewValue);
	UFUNCTION(BlueprintImplementableEvent, Category="BaseAttribute")
	void OnActionPointsChange(float OldValue, float NewValue);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="GameplayTag")
	FGameplayTag GameplayTagCharacter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="GameplayTag")
	FGameplayTag GameplayTagSend;
	

	UFUNCTION(BlueprintCallable)
	void Attack();
};

