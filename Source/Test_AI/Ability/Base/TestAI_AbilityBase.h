﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "Test_AI/Test_AI.h"
#include "TestAI_AbilityBase.generated.h"

/**
 * 
 */
UCLASS()
class TEST_AI_API UTestAI_AbilityBase : public UGameplayAbility
{
	GENERATED_BODY()

public:

	UTestAI_AbilityBase();

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category="Ability")
	EGASAbilityInputID AbilityInputID = EGASAbilityInputID::None;
};
