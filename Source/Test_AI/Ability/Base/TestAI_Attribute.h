﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "TestAI_Attribute.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

UCLASS()
class TEST_AI_API UTestAI_Attribute : public UAttributeSet
{
	GENERATED_BODY()

public:

	UTestAI_Attribute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BaseAttribute")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UTestAI_Attribute, Health)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BaseAttribute")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UTestAI_Attribute, MaxHealth)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BaseAttribute")
	FGameplayAttributeData ActionPoints;
	ATTRIBUTE_ACCESSORS(UTestAI_Attribute, ActionPoints)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BaseAttribute")
	FGameplayAttributeData MaxActionPoints;
	ATTRIBUTE_ACCESSORS(UTestAI_Attribute, MaxActionPoints)

	void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

};
