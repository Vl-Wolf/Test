// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "TestAI_AbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class TEST_AI_API UTestAI_AbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()
	
};
