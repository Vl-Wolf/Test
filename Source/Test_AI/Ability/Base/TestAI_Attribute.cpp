﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TestAI_Attribute.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"


UTestAI_Attribute::UTestAI_Attribute()
{
}

void UTestAI_Attribute::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));
	}

	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetActionPoints(FMath::Clamp(GetActionPoints(), 0.0f, GetMaxActionPoints()));
	}
}
