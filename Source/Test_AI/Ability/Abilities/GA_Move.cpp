﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GA_Move.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Test_AI/Ability/Base/TestAI_Attribute.h"

UGA_Move::UGA_Move()
{
	
}

void UGA_Move::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	
	bool SuccessfullyFound;
    
    	float ActionPoints = UAbilitySystemBlueprintLibrary::GetFloatAttributeFromAbilitySystemComponent(ActorInfo->AbilitySystemComponent.Get(), Attribute, SuccessfullyFound);
    	
    	if (ActionPoints - 1.0f >= 0.0f)
    	{
    		FHitResult HitResult;
    		
    		bool Result = UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHitResultUnderCursor(ECC_Visibility, true, HitResult);
    
    		if (Result)
    		{
    			
    		}
    	}
}

UAbilityTask_ApplyRootMotionMoveToForce* UGA_Move::ApplyRootMotionMoveToForce()
{
	UAbilityTask_ApplyRootMotionMoveToForce* MyObj = nullptr;
	return MyObj;
}
