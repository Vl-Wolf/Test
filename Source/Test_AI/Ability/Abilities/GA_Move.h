﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Test_AI/Ability/Base/TestAI_AbilityBase.h"
#include "Abilities/Tasks/AbilityTask_ApplyRootMotionMoveToForce.h"
#include "GA_Move.generated.h"

/**
 * 
 */
UCLASS()
class TEST_AI_API UGA_Move : public UTestAI_AbilityBase
{
	GENERATED_BODY()

	UGA_Move();

	FApplyRootMotionMoveToForceDelegate OnTimedOut;

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="GameplayAttribute")
	FGameplayAttribute Attribute;

	UFUNCTION(BlueprintCallable, Category="Ability")
	UAbilityTask_ApplyRootMotionMoveToForce* ApplyRootMotionMoveToForce();
};
